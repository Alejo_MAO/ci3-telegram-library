<?php
  class Telegram{
    private $_token = 'BOT_TOKEN';

    // ID CHAT
    private $_receiverGroup = 'ID_CHAT';

    /*
      Otras Variables:
      $content -> Variable para definir el contenido del Mensaje
      $subtitle -> Variable para definir el Caption/Subtitulo/Contenido en texto de la Imagen o Documento Enviado
      $receiver -> Variable para definir el ID del Destinatario (Reemplaza el $_receiverGroup)
    */

    public function __construct($params = array())
    {
      if (count($params) > 0)
      {
        $this->initialize($params);
      }
    }

    private function initialize($params = array())
    {
      if (count($params) > 0)
      {
        foreach ($params as $key => $value)
        {
          if (isset($this->{'_' . $key}))
          {
            $this->{'_' . $key} = $value;
          }
        }
      }
    }

    public function Message($content='',$receiver='')
    {
      $url = "https://api.telegram.org/bot{$this->_token}/sendMessage";

      $message = !empty($content)?$content:'<b>Holaaa</b> <i>Mundo</i>';
      $receiver = !empty($receiver)?$receiver:$this->_receiverGroup;

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "chat_id={$receiver}&parse_mode=HTML&text=$message");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $server_output = curl_exec($ch);
      curl_close($ch);
      return $server_output;
    }

    public function Image($content='',$subtitle="",$receiver='')
    {
      $file = new CURLFile(realpath($content));
      $caption = !empty($subtitle)?$subtitle:'<i>Descripcion de la Imagen</i>';
      $receiver = !empty($receiver)?$receiver:$this->_receiverGroup;

      $url = "https://api.telegram.org/bot{$token}/sendPhoto"; 
      $post = [
        'chat_id' => $receiver,
        'photo' => $file,
        'caption' => $caption
      ]; 
       
      $ch = curl_init(); 
      curl_setopt($ch, CURLOPT_URL, $url); 
      curl_setopt($ch, CURLOPT_POST, 1); 
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
      $server_output = curl_exec($ch); 
      curl_close($ch);
      return $server_output;
    }
  }
